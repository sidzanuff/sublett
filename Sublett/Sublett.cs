﻿using Sublett.OpenSubtitles;
using Sublett.Properties;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Sublett
{
    internal class Sublett
    {
        private const string SUBLETTDB = "sublettdb.xml";
        private const string SUBLETTDB_BAK = "sublettdb.bak";

        private static Sublett instance;

        private string[] movieExtensions = new string[] { "3g2", "", "3gp", "3gp2", "3gpp", "60d", "ajp", "asf", "asx", "avchd", "avi", "bik", "bix", "box", "cam", "dat", "divx", "dmf", "dv", "dvr-ms", "evo", "flc", "fli", "flic", "flv", "flx", "gvi", "gvp", "h264", "m1v", "m2p", "m2ts", "m2v", "m4e", "m4v", "mjp", "mjpeg", "mjpg", "mkv", "moov", "mov", "movhd", "movie", "movx", "mp4", "mpe", "mpeg", "mpg", "mpv", "mpv2", "mxf", "nsv", "nut", "ogg", "ogm", "omf", "ps", "qt", "ram", "rm", "rmvb", "swf", "ts", "vfw", "vid", "video", "viv", "vivo", "vob", "vro", "wm", "wmv", "wmx", "wrap", "wvx", "wx", "x264", "xvid" };

        private SublettDatabase database;

        private Sublett()
        {
        }

        public static Sublett Instance
        {
            get { return instance ?? (instance = new Sublett()); }
        }

        public void Refresh()
        {
            OpenSubtitlesApi openSubtitles = new OpenSubtitlesApi(
                Settings.Default.OpenSubtitleLogin,
                Settings.Default.OpenSubtitlePassword,
                Settings.Default.OpenSubtitleAgent,
                Settings.Default.OpenSubtitleLanguage);

            if (database == null)
            {
                LoadDatabase();
            }

            if (Settings.Default.MovieFolders == null)
            {
                return;
            }

            // remove dead entries
            database.MovieFiles.RemoveAll(f => !File.Exists(f.Path) || !IsFileInLibrary(f.Path));

            foreach (string folder in Settings.Default.MovieFolders)
            {
                FindNewFiles(Directory.GetFiles(folder));
                FindNewFiles(Directory.GetDirectories(folder));
            }

            var hashes = database.MovieFiles.Select(f => f.Hash);
            string[] newHashes = hashes.Where(h => !database.MovieInfos.Any(i => i.MovieHash == h)).ToArray();

            List<MovieInfo> newInfos = null;

            if (newHashes.Any())
            {
                try
                {
                    newInfos = openSubtitles.GetMovieInfo(newHashes);
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }

            if (newInfos != null)
            {
                database.MovieInfos.AddRange(newInfos);
            }

            var unconfirmed = database.MovieFiles.Where(f => !f.Confirmed);

            foreach (MovieFile file in unconfirmed)
            {
                var infos = database.MovieInfos.Where(i => i.MovieHash == file.Hash);

                foreach (var info in infos)
                {
                    if (MatchMovieName(Path.GetFileName(file.Path), info.MovieName))
                    {
                        file.ImdbID = info.MovieImdbID;
                        file.Confirmed = true;
                        break;
                    }
                }

                if (!file.Confirmed && infos.Any())
                {
                    file.ImdbID = infos.First().MovieImdbID;
                }
            }

            SaveDatabase();
        }

        private static bool IsFileInLibrary(string path)
        {
            string fileDir = Path.GetDirectoryName(path);
            foreach (string dir in Settings.Default.MovieFolders)
            {
                if (fileDir.Contains(dir))
                {
                    return true;
                }
            }
            return false;
        }

        public List<MovieListItem> GetMovies()
        {
            List<MovieListItem> items = new List<MovieListItem>();

            foreach (MovieFile file in database.MovieFiles)
            {
                MovieListItem item = new MovieListItem();

                item.Confirmed = file.Confirmed ? "Confirmed" : "";
                item.Path = file.Path;
                item.Tags = file.Tags;

                if (file.ImdbID != null)
                {
                    var info = database.MovieInfos.First(i => i.MovieImdbID == file.ImdbID);
                    item.Name = info.MovieName;
                    item.Kind = info.MovieKind;
                    item.Season = info.SeriesSeason;
                    item.Episode = info.SeriesEpisode;
                }
                else
                {
                    item.Name = Path.GetFileNameWithoutExtension(file.Path);
                }

                item.Name = item.Name.Replace("\"", "");

                if (string.IsNullOrEmpty(item.Kind))
                {
                    item.Kind = "unknown";
                }

                item.Tags = file.Tags;

                items.Add(item);
            }

            return items;
        }

        public void UpdateTags(Dictionary<string, string> pathTags)
        {
            foreach (string path in pathTags.Keys)
            {
                database.MovieFiles.First(m => m.Path == path).Tags = pathTags[path];
            }

            SaveDatabase();
        }

        public List<MovieInfo> GetMatchingIMDbEntries(string path)
        {
            MovieFile file = database.MovieFiles.First(f => f.Path == path);
            return database.MovieInfos.Where(i => i.MovieHash == file.Hash).ToList();
        }

        public void UpdateMovie(string path, string imdbID)
        {
            MovieFile file = database.MovieFiles.First(f => f.Path == path);
            file.ImdbID = imdbID;
            file.Confirmed = true;
            SaveDatabase();
        }

        private void LoadDatabase()
        {
            if (!File.Exists(SUBLETTDB))
            {
                database = new SublettDatabase();
            }
            else
            {
                using (StreamReader streamReader = new StreamReader(SUBLETTDB))
                {
                    database = (SublettDatabase)new XmlSerializer(typeof(SublettDatabase)).Deserialize(streamReader);
                }
            }
        }

        private void FindNewFiles(string[] files)
        {
            foreach (string file in files)
            {
                if (Directory.Exists(file))
                {
                    FindNewFiles(Directory.GetFiles(file));
                    FindNewFiles(Directory.GetDirectories(file));
                }
                else
                {
                    string ext = Path.GetExtension(file).ToLower().Replace(".", "");

                    if (movieExtensions.Contains(ext))
                    {
                        MovieFile movieFile = database.MovieFiles.FirstOrDefault(f => f.Path == file);

                        if (movieFile == null)
                        {
                            movieFile = new MovieFile();
                            movieFile.Path = file;
                            database.MovieFiles.Add(movieFile);
                        }

                        byte[] hash = OpenSubtitles.Hash.ComputeMovieHash(file);
                        string hashString = OpenSubtitles.Hash.ToHexadecimal(hash);
                        movieFile.Hash = hashString;
                    }
                }
            }
        }

        private static bool MatchMovieName(string fileName, string movieName)
        {
            fileName = fileName.ToLower();
            string[] titleParts = GetNameParts(movieName);
            foreach (string part in titleParts)
            {
                if (part == "the")
                {
                    continue;
                }
                if (!fileName.Contains(part))
                {
                    return false;
                }
            }
            return true;
        }

        private static string[] GetNameParts(string name)
        {
            name = name.ToLower();
            StringBuilder sb = new StringBuilder();
            List<string> parts = new List<string>();

            for (int i = 0; i < name.Length; i++)
            {
                char c = name[i];

                if (Char.IsLetter(c))
                {
                    if (i > 0 && !Char.IsLetter(name[i - 1]) && sb.Length > 0)
                    {
                        parts.Add(sb.ToString());
                        sb.Clear();
                    }
                    sb.Append(c);
                }
                else if (Char.IsDigit(c))
                {
                    if (i > 0 && !Char.IsDigit(name[i - 1]) && sb.Length > 0)
                    {
                        parts.Add(sb.ToString());
                        sb.Clear();
                    }
                    sb.Append(c);
                }
            }

            if (sb.Length > 0)
            {
                parts.Add(sb.ToString());
            }

            return parts.ToArray();
        }

        private void SaveDatabase()
        {
            if (File.Exists(SUBLETTDB))
            {
                if (File.Exists(SUBLETTDB_BAK))
                {
                    File.Delete(SUBLETTDB_BAK);
                    File.Move(SUBLETTDB, SUBLETTDB_BAK);
                }
            }

            using (StreamWriter streamWriter = new StreamWriter(SUBLETTDB))
            {
                new XmlSerializer(typeof(SublettDatabase)).Serialize(streamWriter, database);
            }
        }
    }
}