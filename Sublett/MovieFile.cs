﻿namespace Sublett
{
    public class MovieFile
    {
        public string Path { get; set; }

        public string Hash { get; set; }

        public string ImdbID { get; set; }

        public bool Confirmed { get; set; }

        public string Tags { get; set; }
    }
}