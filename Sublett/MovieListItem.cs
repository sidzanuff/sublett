﻿namespace Sublett
{
    public class MovieListItem
    {
        public string Name { get; set; }

        public int Season { get; set; }

        public int Episode { get; set; }

        public string Tags { get; set; }

        public string Kind { get; set; }

        public string Confirmed { get; set; }

        public string Path { get; set; }
    }
}