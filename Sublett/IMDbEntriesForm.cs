﻿using Sublett.OpenSubtitles;
using System;
using System.Collections;
using System.Windows.Forms;

namespace Sublett
{
    public partial class IMDbEntriesForm : Form
    {
        public MovieListItem Movie { get; set; }

        public IMDbEntriesForm()
        {
            InitializeComponent();
        }

        private void IMDbEntriesForm_Load(object sender, EventArgs e)
        {
            resultDataGridView.DataSource = new SortableBindingList<MovieInfo>(Sublett.Instance.GetMatchingIMDbEntries(Movie.Path));
        }

        private void selectButton_Click(object sender, EventArgs e)
        {
            MovieInfo info = resultDataGridView.SelectedRows[0].DataBoundItem as MovieInfo;
            Sublett.Instance.UpdateMovie(Movie.Path, info.MovieImdbID);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void noneButton_Click(object sender, EventArgs e)
        {
            Sublett.Instance.UpdateMovie(Movie.Path, null);
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }

        private void resultDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            selectButton.Enabled = resultDataGridView.SelectedRows.Count == 1;
        }
    }
}