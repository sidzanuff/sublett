﻿using Sublett.OpenSubtitles;
using System.Collections.Generic;

namespace Sublett
{
    public class SublettDatabase
    {
        public List<MovieFile> MovieFiles { get; set; }

        public List<MovieInfo> MovieInfos { get; set; }

        public SublettDatabase()
        {
            MovieFiles = new List<MovieFile>();
            MovieInfos = new List<MovieInfo>();
        }
    }
}