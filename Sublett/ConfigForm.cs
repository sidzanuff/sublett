﻿using Sublett.Properties;
using System;
using System.Windows.Forms;

namespace Sublett
{
    public partial class ConfigForm : Form
    {
        public ConfigForm()
        {
            InitializeComponent();

            loginTextBox.Text = Settings.Default.OpenSubtitleLogin;
            passTextBox.Text = Settings.Default.OpenSubtitlePassword;
            agentTextBox.Text = Settings.Default.OpenSubtitleAgent;
            langTextBox.Text = Settings.Default.OpenSubtitleLanguage;
            if (Settings.Default.MovieFolders != null)
            {
                foreach (string folder in Settings.Default.MovieFolders)
                {
                    foldersListView.Items.Add(folder);
                }
            }
        }

        private void foldersListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            removeButton.Enabled = foldersListView.SelectedItems.Count > 0;
        }

        private void addButton_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            if (dialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foldersListView.Items.Add(dialog.SelectedPath);
            }
        }

        private void removeButton_Click(object sender, EventArgs e)
        {
            var selectedItems = foldersListView.SelectedItems;
            foreach (ListViewItem item in selectedItems)
            {
                foldersListView.Items.Remove(item);
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            Settings.Default.OpenSubtitleLogin = loginTextBox.Text;
            Settings.Default.OpenSubtitlePassword = passTextBox.Text;
            Settings.Default.OpenSubtitleLanguage = langTextBox.Text;
            Settings.Default.OpenSubtitleAgent = agentTextBox.Text;
            Settings.Default.MovieFolders = new System.Collections.Specialized.StringCollection();
            foreach (ListViewItem item in foldersListView.Items)
            {
                Settings.Default.MovieFolders.Add(item.Text);
            }
            Settings.Default.Save();
            DialogResult = System.Windows.Forms.DialogResult.OK;
        }
    }
}