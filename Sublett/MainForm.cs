﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Windows.Forms;

namespace Sublett
{
    public partial class MainForm : Form
    {
        private List<MovieListItem> movies;
        private bool filling;

        public MainForm()
        {
            InitializeComponent();

            typeof(DataGridView).InvokeMember("DoubleBuffered", BindingFlags.NonPublic |
                BindingFlags.Instance | BindingFlags.SetProperty, null,
                moviesDataGridView, new object[] { true });
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            RefreshMovies();
        }

        private void configButton_Click(object sender, EventArgs e)
        {
            if (new ConfigForm().ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                RefreshMovies();
            }
        }

        private void filterTextBox_TextChanged(object sender, EventArgs e)
        {
            ApplyFilter();
        }

        private void kindListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!filling)
            {
                ApplyFilter();
            }
        }

        private void deleteFileButton_Click(object sender, EventArgs e)
        {
            var paths = moviesDataGridView
                .SelectedRows.Cast<DataGridViewRow>()
                .Select(r => r.DataBoundItem as MovieListItem)
                .Select(m => m.Path);

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("Delete following files?").AppendLine();
            paths.ToList().ForEach(p => sb.AppendLine(p));

            if (MessageBox.Show(sb.ToString(), "", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) ==
                DialogResult.OK)
            {
                foreach (string path in paths)
                {
                    try
                    {
                        File.Delete(path);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        return;
                    }
                }
            }

            RefreshMovies();
        }

        private void moviesDataGridView_SelectionChanged(object sender, EventArgs e)
        {
            deleteFileButton.Enabled = movieTagListView.Enabled = addTagButton.Enabled = moviesDataGridView.SelectedRows.Count > 0;
            filling = true;
            RefreshMovieTags();
            filling = false;
        }

        private void tagFilterListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!filling)
            {
                ApplyFilter();
            }
        }

        private void movieTagListView_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (!filling)
            {
                filling = true;
                if (e.Item.Checked)
                {
                    AddTagToSelectedMovies(e.Item.Text);
                }
                else
                {
                    RemoveTagFromSelectedMovies(e.Item.Text);
                }
                filling = false;
                moviesDataGridView.Refresh();
            }
        }

        private void addTagButton_Click(object sender, EventArgs e)
        {
            AddNewTag();
        }

        private void addTagTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
                AddNewTag();
            }
        }

        private void moviesDataGridView_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex >= 0 && e.ColumnIndex >= 0)
            {
                MovieListItem item = moviesDataGridView.Rows[e.RowIndex].DataBoundItem as MovieListItem;
                new Process { StartInfo = new ProcessStartInfo(item.Path) }.Start();
            }
        }

        private void assignButton_Click(object sender, EventArgs e)
        {
            IMDbEntriesForm form = new IMDbEntriesForm();
            form.Movie = moviesDataGridView.SelectedRows[0].DataBoundItem as MovieListItem;
            if (form.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                RefreshMovies();
            }
        }

        private void refreshButton_Click(object sender, EventArgs e)
        {
            RefreshMovies();
        }

        private void AddNewTag()
        {
            string tag = addTagTextBox.Text;

            if (!string.IsNullOrEmpty(tag))
            {
                AddTagToSelectedMovies(tag);
            }

            addTagTextBox.Text = "";

            moviesDataGridView.Refresh();
            filling = true;
            FillTagFilter();
            RefreshMovieTags();
            filling = false;
        }

        private void AddTagToSelectedMovies(string tag)
        {
            List<MovieListItem> items = moviesDataGridView
                    .SelectedRows
                    .Cast<DataGridViewRow>()
                    .Select(r => r.DataBoundItem as MovieListItem)
                    .ToList();

            Dictionary<string, string> pathTags = new Dictionary<string, string>();

            foreach (var item in items)
            {
                AddTagToMovie(item, tag);
                pathTags[item.Path] = item.Tags;
            }

            Sublett.Instance.UpdateTags(pathTags);
        }

        private void RemoveTagFromSelectedMovies(string tag)
        {
            List<MovieListItem> items = moviesDataGridView
                    .SelectedRows
                    .Cast<DataGridViewRow>()
                    .Select(r => r.DataBoundItem as MovieListItem)
                    .ToList();

            Dictionary<string, string> pathTags = new Dictionary<string, string>();

            foreach (var item in items)
            {
                RemoveTagFromMovie(item, tag);
                pathTags[item.Path] = item.Tags;
            }

            Sublett.Instance.UpdateTags(pathTags);
        }

        private void AddTagToMovie(MovieListItem item, string tag)
        {
            item.Tags = item.Tags + " " + tag.Replace(' ', '_');
            FixMovieTags(item);
        }

        private void RemoveTagFromMovie(MovieListItem item, string tag)
        {
            if (item.Tags != null)
            {
                item.Tags = item.Tags.Replace(tag, "");
                FixMovieTags(item);
            }
        }

        private void FixMovieTags(MovieListItem item)
        {
            string[] tags = item.Tags.Split(' ');
            tags = tags.Distinct().OrderBy(t => t).ToArray();
            item.Tags = string.Join(" ", tags);
        }

        private void RefreshMovies()
        {
            Sublett.Instance.Refresh();
            movies = Sublett.Instance.GetMovies().OrderBy(m => m.Name).ToList();
            filling = true;
            FillKinds();
            FillTagFilter();
            filling = false;
            ApplyFilter();
        }

        private void FillKinds()
        {
            string[] selectedKinds = kindListView.Items.Cast<ListViewItem>().Where(i => i.Checked).Select(i => i.Text).ToArray();

            kindListView.Items.Clear();
            var kinds = movies.Select(m => m.Kind).Distinct().OrderBy(k => k);

            foreach (string kind in kinds)
            {
                ListViewItem item = kindListView.Items.Add(kind);
                if (selectedKinds.Any())
                {
                    item.Checked = selectedKinds.Contains(kind);
                }
                else
                {
                    item.Checked = true;
                }
            }
        }

        private void FillTagFilter()
        {
            //string[] unselectedTags = tagFilterListView.Items
            //    .Cast<ListViewItem>()
            //    .Where(i => !i.Checked)
            //    .Select(i => i.Text)
            //    .ToArray();

            tagFilterListView.Items.Clear();
            movieTagListView.Items.Clear();

            string[] tags = movies
                .Where(m => !string.IsNullOrEmpty(m.Tags))
                .SelectMany(m => m.Tags.Split(' '))
                .Where(t => !string.IsNullOrEmpty(t))
                .Distinct()
                .OrderBy(t => t)
                .ToArray();

            foreach (string tag in tags)
            {
                ListViewItem item = tagFilterListView.Items.Add(tag);
                //item.Checked = !unselectedTags.Contains(tag);

                movieTagListView.Items.Add(tag);
            }
        }

        private void RefreshMovieTags()
        {
            string[] selectedTags = moviesDataGridView
                .SelectedRows
                .Cast<DataGridViewRow>()
                .Select(r => r.DataBoundItem as MovieListItem)
                .Where(m => !string.IsNullOrEmpty(m.Tags))
                .SelectMany(m => m.Tags.Split(' '))
                .Distinct()
                .ToArray();

            foreach (ListViewItem item in movieTagListView.Items)
            {
                item.Checked = selectedTags.Contains(item.Text);
            }
        }

        private void ApplyFilter()
        {
            filling = true;

            var selectedKinds = kindListView.Items.Cast<ListViewItem>().Where(i => i.Checked).Select(i => i.Text);
            IEnumerable<MovieListItem> filtered = movies.Where(m => selectedKinds.Contains(m.Kind));

            string[] selectedTags = tagFilterListView.CheckedItems.Cast<ListViewItem>().Select(i => i.Text).ToArray();
            if (selectedTags.Any())
            {
                filtered = filtered.Where(m => ContainsTag(m, selectedTags));
            }

            if (!selectedTags.Contains("hide"))
            {
                filtered = filtered.Where(m => !ContainsTag(m, new string[] { "hide" }));
            }

            if (!String.IsNullOrEmpty(filterTextBox.Text))
            {
                filtered = movies.Where(m => m.Name.ToLower().Contains(filterTextBox.Text.ToLower())).ToList();
            }

            moviesDataGridView.DataSource = new SortableBindingList<MovieListItem>(filtered.ToList());

            filling = false;
        }

        private static bool ContainsTag(MovieListItem movie, string[] selectedTags)
        {
            if (movie.Tags == null)
            {
                return false;
            }

            string[] movieTags = movie.Tags.Split(' ');

            foreach (string tag in selectedTags)
            {
                if (!movieTags.Contains(tag))
                {
                    return false;
                }
            }

            return true;
        }
    }
}