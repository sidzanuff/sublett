﻿namespace Sublett
{
    public class test
    {
        public string FileName { get; set; }

        public string Hash { get; set; }

        public int Matches { get; set; }

        public string Title { get; set; }

        public string Path { get; set; }
    }
}