﻿namespace Sublett
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.moviesDataGridView = new System.Windows.Forms.DataGridView();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.filterTextBox = new System.Windows.Forms.ToolStripTextBox();
            this.deleteFileButton = new System.Windows.Forms.ToolStripButton();
            this.configButton = new System.Windows.Forms.ToolStripButton();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addTagButton = new System.Windows.Forms.Button();
            this.tagFilterListView = new System.Windows.Forms.ListView();
            this.addTagTextBox = new System.Windows.Forms.TextBox();
            this.kindListView = new System.Windows.Forms.ListView();
            this.movieTagListView = new System.Windows.Forms.ListView();
            this.label1 = new System.Windows.Forms.Label();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.assignButton = new System.Windows.Forms.ToolStripButton();
            this.refreshButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            ((System.ComponentModel.ISupportInitialize)(this.moviesDataGridView)).BeginInit();
            this.toolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // moviesDataGridView
            // 
            this.moviesDataGridView.AllowUserToAddRows = false;
            this.moviesDataGridView.AllowUserToDeleteRows = false;
            this.moviesDataGridView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.moviesDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.moviesDataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moviesDataGridView.Location = new System.Drawing.Point(0, 0);
            this.moviesDataGridView.Name = "moviesDataGridView";
            this.moviesDataGridView.ReadOnly = true;
            this.moviesDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.moviesDataGridView.Size = new System.Drawing.Size(794, 704);
            this.moviesDataGridView.TabIndex = 0;
            this.moviesDataGridView.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.moviesDataGridView_CellMouseDoubleClick);
            this.moviesDataGridView.SelectionChanged += new System.EventHandler(this.moviesDataGridView_SelectionChanged);
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.refreshButton,
            this.toolStripSeparator2,
            this.toolStripLabel1,
            this.filterTextBox,
            this.toolStripSeparator1,
            this.deleteFileButton,
            this.configButton,
            this.toolStripSeparator3,
            this.assignButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1008, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(69, 22);
            this.toolStripLabel1.Text = "Name filter:";
            // 
            // filterTextBox
            // 
            this.filterTextBox.Name = "filterTextBox";
            this.filterTextBox.Size = new System.Drawing.Size(200, 25);
            this.filterTextBox.TextChanged += new System.EventHandler(this.filterTextBox_TextChanged);
            // 
            // deleteFileButton
            // 
            this.deleteFileButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.deleteFileButton.Enabled = false;
            this.deleteFileButton.Image = ((System.Drawing.Image)(resources.GetObject("deleteFileButton.Image")));
            this.deleteFileButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.deleteFileButton.Name = "deleteFileButton";
            this.deleteFileButton.Size = new System.Drawing.Size(65, 22);
            this.deleteFileButton.Text = "Delete File";
            this.deleteFileButton.Click += new System.EventHandler(this.deleteFileButton_Click);
            // 
            // configButton
            // 
            this.configButton.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.configButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.configButton.Image = ((System.Drawing.Image)(resources.GetObject("configButton.Image")));
            this.configButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.configButton.Name = "configButton";
            this.configButton.Size = new System.Drawing.Size(47, 22);
            this.configButton.Text = "Config";
            this.configButton.Click += new System.EventHandler(this.configButton_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 25);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.moviesDataGridView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.label3);
            this.splitContainer1.Panel2.Controls.Add(this.label2);
            this.splitContainer1.Panel2.Controls.Add(this.addTagButton);
            this.splitContainer1.Panel2.Controls.Add(this.tagFilterListView);
            this.splitContainer1.Panel2.Controls.Add(this.addTagTextBox);
            this.splitContainer1.Panel2.Controls.Add(this.kindListView);
            this.splitContainer1.Panel2.Controls.Add(this.movieTagListView);
            this.splitContainer1.Panel2.Controls.Add(this.label1);
            this.splitContainer1.Size = new System.Drawing.Size(1008, 704);
            this.splitContainer1.SplitterDistance = 794;
            this.splitContainer1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 238);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(106, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Selected movie tags:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 119);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Tag filter:";
            // 
            // addTagButton
            // 
            this.addTagButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.addTagButton.Location = new System.Drawing.Point(170, 360);
            this.addTagButton.Name = "addTagButton";
            this.addTagButton.Size = new System.Drawing.Size(37, 23);
            this.addTagButton.TabIndex = 2;
            this.addTagButton.Text = "Add";
            this.addTagButton.UseVisualStyleBackColor = true;
            this.addTagButton.Click += new System.EventHandler(this.addTagButton_Click);
            // 
            // tagFilterListView
            // 
            this.tagFilterListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tagFilterListView.CheckBoxes = true;
            this.tagFilterListView.Location = new System.Drawing.Point(3, 135);
            this.tagFilterListView.Name = "tagFilterListView";
            this.tagFilterListView.Size = new System.Drawing.Size(204, 100);
            this.tagFilterListView.TabIndex = 0;
            this.tagFilterListView.UseCompatibleStateImageBehavior = false;
            this.tagFilterListView.View = System.Windows.Forms.View.List;
            this.tagFilterListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.tagFilterListView_ItemChecked);
            // 
            // addTagTextBox
            // 
            this.addTagTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.addTagTextBox.Location = new System.Drawing.Point(3, 360);
            this.addTagTextBox.Name = "addTagTextBox";
            this.addTagTextBox.Size = new System.Drawing.Size(161, 20);
            this.addTagTextBox.TabIndex = 1;
            this.addTagTextBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.addTagTextBox_KeyDown);
            // 
            // kindListView
            // 
            this.kindListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.kindListView.CheckBoxes = true;
            this.kindListView.Location = new System.Drawing.Point(3, 16);
            this.kindListView.Name = "kindListView";
            this.kindListView.Size = new System.Drawing.Size(204, 100);
            this.kindListView.TabIndex = 0;
            this.kindListView.UseCompatibleStateImageBehavior = false;
            this.kindListView.View = System.Windows.Forms.View.List;
            this.kindListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.kindListView_ItemChecked);
            // 
            // movieTagListView
            // 
            this.movieTagListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.movieTagListView.CheckBoxes = true;
            this.movieTagListView.Location = new System.Drawing.Point(3, 254);
            this.movieTagListView.Name = "movieTagListView";
            this.movieTagListView.Size = new System.Drawing.Size(204, 100);
            this.movieTagListView.TabIndex = 0;
            this.movieTagListView.UseCompatibleStateImageBehavior = false;
            this.movieTagListView.View = System.Windows.Forms.View.List;
            this.movieTagListView.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.movieTagListView_ItemChecked);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Kind filter:";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // assignButton
            // 
            this.assignButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.assignButton.Image = ((System.Drawing.Image)(resources.GetObject("assignButton.Image")));
            this.assignButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.assignButton.Name = "assignButton";
            this.assignButton.Size = new System.Drawing.Size(108, 22);
            this.assignButton.Text = "Assign IMDb Entry";
            this.assignButton.Click += new System.EventHandler(this.assignButton_Click);
            // 
            // refreshButton
            // 
            this.refreshButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.refreshButton.Image = ((System.Drawing.Image)(resources.GetObject("refreshButton.Image")));
            this.refreshButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.refreshButton.Name = "refreshButton";
            this.refreshButton.Size = new System.Drawing.Size(50, 22);
            this.refreshButton.Text = "Refresh";
            this.refreshButton.Click += new System.EventHandler(this.refreshButton_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.toolStrip1);
            this.Name = "MainForm";
            this.Text = "Sublett";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.moviesDataGridView)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView moviesDataGridView;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton configButton;
        private System.Windows.Forms.ToolStripTextBox filterTextBox;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.ListView kindListView;
        private System.Windows.Forms.ToolStripButton deleteFileButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ListView tagFilterListView;
        private System.Windows.Forms.Button addTagButton;
        private System.Windows.Forms.TextBox addTagTextBox;
        private System.Windows.Forms.ListView movieTagListView;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton assignButton;
        private System.Windows.Forms.ToolStripButton refreshButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    }
}

