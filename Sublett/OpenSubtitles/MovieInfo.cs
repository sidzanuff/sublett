﻿namespace Sublett.OpenSubtitles
{
    public class MovieInfo
    {
        public string MovieHash { get; set; }

        public string MovieImdbID { get; set; }

        public string MovieName { get; set; }

        public string MovieYear { get; set; }

        public string MovieKind { get; set; }

        public int SeriesSeason { get; set; }

        public int SeriesEpisode { get; set; }

        public int SeenCount { get; set; }

        public int SubCount { get; set; }
    }
}