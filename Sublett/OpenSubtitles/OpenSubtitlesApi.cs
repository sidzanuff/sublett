﻿using CookComputing.XmlRpc;
using System;
using System.Collections.Generic;

namespace Sublett.OpenSubtitles
{
    public class OpenSubtitlesApi
    {
        private string login;
        private string password;
        private string agent;
        private string language;

        private IOpenSubtitlesService proxy;
        private string token;

        public OpenSubtitlesApi(string login, string password, string agent, string language)
        {
            this.login = login;
            this.password = password;
            this.agent = agent;
            this.language = language;

            proxy = XmlRpcProxyGen.Create<IOpenSubtitlesService>();
        }

        public List<MovieInfo> GetMovieInfo(params string[] hashes)
        {
            LoginResponse loginResponse = proxy.LogIn(login, password, language, agent);

            if (loginResponse.status != "200 OK")
            {
                throw new Exception(loginResponse.status);
            }

            token = loginResponse.token;

            List<MovieInfo> infos = null;

            try
            {
                infos = TryGetMovieInfo(hashes);
            }
            finally
            {
                proxy.LogOut(token);
            }

            return infos;
        }

        private List<MovieInfo> TryGetMovieInfo(params string[] hashes)
        {
            CheckMovieResponse searchResponse = proxy.CheckMovieHash2(token, hashes);
            List<MovieInfo> infos = null;

            if (searchResponse.data is XmlRpcStruct)
            {
                infos = ParseSearchResponse((XmlRpcStruct)searchResponse.data);
            }

            return infos;
        }

        private static List<MovieInfo> ParseSearchResponse(XmlRpcStruct rpcStruct)
        {
            List<MovieInfo> infos = new List<MovieInfo>();

            foreach (object[] s in rpcStruct.Values)
            {
                foreach (XmlRpcStruct t in s)
                {
                    infos.Add(ParseMovieInfo(t));
                }
            }

            return infos;
        }

        private static MovieInfo ParseMovieInfo(XmlRpcStruct rpcStruct)
        {
            MovieInfo movieInfo = new MovieInfo();
            movieInfo.MovieHash = (string)rpcStruct["MovieHash"];
            movieInfo.MovieImdbID = (string)rpcStruct["MovieImdbID"];
            movieInfo.MovieName = (string)rpcStruct["MovieName"];
            movieInfo.MovieYear = (string)rpcStruct["MovieYear"];
            movieInfo.MovieKind = (string)rpcStruct["MovieKind"];
            movieInfo.SeriesSeason = TryParseInt(rpcStruct["SeriesSeason"]);
            movieInfo.SeriesEpisode = TryParseInt(rpcStruct["SeriesEpisode"]);
            movieInfo.SeenCount = TryParseInt(rpcStruct["SeenCount"]);
            movieInfo.SubCount = TryParseInt(rpcStruct["SubCount"]);
            return movieInfo;
        }

        private static int TryParseInt(object o)
        {
            try
            {
                return Convert.ToInt32(o);
            }
            catch
            {
                return 0;
            }
        }
    }
}