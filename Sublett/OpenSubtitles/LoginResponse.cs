﻿using CookComputing.XmlRpc;

namespace Sublett
{
    public class LoginResponse
    {
        [XmlRpcMissingMapping(MappingAction.Ignore)]
        public string token;

        public string status;
        //public double seconds;
    }
}