﻿using CookComputing.XmlRpc;

namespace Sublett.OpenSubtitles
{
    [XmlRpcUrl("http://api.opensubtitles.org/xml-rpc")]
    public interface IOpenSubtitlesService
    {
        [XmlRpcMethod("LogIn")]
        LoginResponse LogIn(string username, string password, string language, string useragent);

        [XmlRpcMethod("CheckMovieHash2")]
        CheckMovieResponse CheckMovieHash2(string token, string[] hashes);

        [XmlRpcMethod("LogOut")]
        LoginResponse LogOut(string token);
    }
}